﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using Lean.Pool;
using System;
using UnityEngine.SceneManagement;
public class SceneLoaderManager : MonoBehaviour {
    public static SceneLoaderManager instance{
		get{
			if(ins == null){
				ins = Instantiate(CoreGameManager.instance.singletonPrefabInfo.sceneLoaderManagerPrefab);
				DontDestroyOnLoad (ins.gameObject);
			}
			return ins;
		}
	}
	private static SceneLoaderManager ins;
	public static bool IsExist(){
		if(ins != null){
			return true;
		}
		return false;
	}

	public enum State{
		Hide,
		Show
	}
	public State currentState{ get; set;}

	[SerializeField] CanvasGroup myCanvasGroup;
	[SerializeField] Canvas myCanvas;
	[SerializeField] Image imgBg;
	[SerializeField] Slider loadingBar;

	[ReadOnly] public string lastSceneName;
	[ReadOnly] public string currentSceneName;

	int idTweenMyCanvasGroup = -1;
	// [SerializeField] GraphicRaycaster gpRaycaster;
	// [SerializeField] GameObject loadingParticle;
	

	void Awake() {
		if (ins != null && ins != this) {
			Destroy(this.gameObject);
			return;
		}
		lastSceneName = currentSceneName = string.Empty;
		Hide();
	}

	[ContextMenu("Show")]
	void Show(bool _updateNow = true, System.Action _onFinished = null){
		if(idTweenMyCanvasGroup != -1 && LeanTween.descr(idTweenMyCanvasGroup) != null){
			LeanTween.cancel(idTweenMyCanvasGroup);
		}
		idTweenMyCanvasGroup = -1;
		
		currentState = State.Show;
		myCanvasGroup.blocksRaycasts = true;
		
		if(_updateNow){
			myCanvasGroup.alpha = 1f;
			if(_onFinished != null){
				_onFinished();
			}
		}else{
			idTweenMyCanvasGroup = LeanTween.alphaCanvas(myCanvasGroup, 1f, 0.2f).setOnComplete(()=>{
				idTweenMyCanvasGroup = -1;
				if(_onFinished != null){
					_onFinished();
				}
			}).id;
		}
	}

	void Hide(bool _updateNow = true, System.Action _onFinished = null){
		if(idTweenMyCanvasGroup != -1 && LeanTween.descr(idTweenMyCanvasGroup) != null){
			LeanTween.cancel(idTweenMyCanvasGroup);
		}
		idTweenMyCanvasGroup = -1;

		currentState = State.Hide;
		myCanvasGroup.blocksRaycasts = false;
		if(_updateNow){
			myCanvasGroup.alpha = 0f;
			if(_onFinished != null){
				_onFinished();
			}
		}else{
			idTweenMyCanvasGroup = LeanTween.alphaCanvas(myCanvasGroup, 0f, 0.2f).setOnComplete(()=>{
				idTweenMyCanvasGroup = -1;
				if(_onFinished != null){
					_onFinished();
				}
			}).id;
		}
	}
	public Coroutine LoadScene(SceneIndex _scene){
		lastSceneName = currentSceneName;
		currentSceneName = _scene.ToString();
		loadingBar.value = loadingBar.minValue;
		
		return StartCoroutine(DoActionLoadScene(currentSceneName));
	}
	public void SetSpriteBg(Sprite _spriteBg){
		if(_spriteBg != null){
			imgBg.sprite = _spriteBg;
		}
	}
	IEnumerator DoActionLoadScene(string _nameScene){
		bool _isFinished = false;

		Show(false, ()=>{
			_isFinished = true;
		});
		yield return new WaitUntil(()=>_isFinished);
		yield return Yielders.EndOfFrame;
		
		DateTime _timeStart = DateTime.UtcNow;
		LeanTween.cancelAll(true);
		PopupManager.Instance.DestroyAll();
		CoreGameManager.instance.ClearAllCallbackPressBackKey();
		LeanPool.DespawnAll();
		if (CoreGameManager.instance.callbackManager.onDestroyAllSingletonCreated != null){
			CoreGameManager.instance.callbackManager.onDestroyAllSingletonCreated();
			CoreGameManager.instance.callbackManager.onDestroyAllSingletonCreated = null;
		}
		CoreGameManager.instance.callbackManager.onUpdateOwnGold = null;
		
		yield return Yielders.EndOfFrame;

		var _asyncLoad = SceneManager.LoadSceneAsync (_nameScene, LoadSceneMode.Single);

		while (!_asyncLoad.isDone){
			yield return null;
            loadingBar.value = Mathf.RoundToInt(_asyncLoad.progress) * 100f;
        }
		loadingBar.value = loadingBar.maxValue;
		
		if(myCanvas.worldCamera == null){
			myCanvas.worldCamera = Camera.main;
		}

		yield return Resources.UnloadUnusedAssets ();

		// yield return new WaitUntil (()=>CoreGameManager.instance.currentSceneManager != null && CoreGameManager.instance.currentSceneManager.canShowScene);

		yield return Yielders.EndOfFrame;
		
		long _timeLoadScene = (long) (DateTime.UtcNow - _timeStart).TotalMilliseconds;
		
		while(_timeLoadScene < 1000){
			yield return null;
			_timeLoadScene = (long) (DateTime.UtcNow - _timeStart).TotalMilliseconds;
		}
		// AudioManager.instance.isStopPlayingNewSound = false;
		Hide(false);
		yield break;
	}
}
