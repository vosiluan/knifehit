﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using NaughtyAttributes;

public class PopupManager : MonoBehaviour {
	public static PopupManager Instance{
		get{
			if(ins == null){
				ins = Instantiate(CoreGameManager.instance.singletonPrefabInfo.popupManagerPrefab);
				DontDestroyOnLoad (ins.gameObject);
			}
			return ins;
		}
	}
    static PopupManager ins;

	[SerializeField] Canvas myCanvas;
    [SerializeField] CanvasGroup popupCanvasGroup;
	[SerializeField] Transform popupContainer;
	[SerializeField] CanvasGroup notificationCanvasGroup;
	[SerializeField] Transform notificationContainer;
	[SerializeField] Transform pool;
	[SerializeField] ToastController toastPrefab;
	[SerializeField] PopupFinishGameController popupFinishGamePrefab;
	[SerializeField] PopupKnifeShopController popupKnifeShopPrefab;
	[SerializeField] PopupSettingController popupSettingPrefab;
	[Header("Prefabs")]
	List<ToastController> toasts;
	List<IPopupController> popupsActive;
	void Awake() {
		if (ins != null && ins != this) { 
			Destroy(this.gameObject);
			return;
		}
		popupCanvasGroup.alpha = 1f;
		popupCanvasGroup.blocksRaycasts = false;
		if (toasts == null){
			toasts = new List<ToastController>();
		}
		if (popupsActive == null){
			popupsActive = new List<IPopupController>();
		}
	}
    /**
	 * AddPopupActive : add các popup đang active vào list
	 * */
	void AddPopupActive(IPopupController _popup){
		_popup.transform.SetAsLastSibling();
		popupsActive.Add(_popup);
	}
	/**
	 * RemovePopupActive : remove các popup đang active ra khỏi list
	 * */
	public void CreateToast(string _textInfo)
	{
		CreateToast(_textInfo, ToastController.posDefault, ToastController.colorTextDefault, ToastController.maxSizeWDefault);
	}
	public void CreateToast(string _textInfo, Vector3 _pos, Color _colorText, float _maxSizeW, bool _playSfx = true)
	{
		SetCanvasPopupAgain();

		ToastController _tmpPopup = LeanPool.Spawn(toastPrefab, Vector3.zero, Quaternion.identity, popupContainer, pool);
		_tmpPopup.transform.position = _pos;
		_tmpPopup.Init(_textInfo, _colorText, _maxSizeW, () => {
			if (toasts != null && toasts.Count > 0)
			{
				toasts.Remove(_tmpPopup);
			}
			RemovePopupActive(_tmpPopup);
		});
		// if(_playSfx){
		//     MyAudioManager.instance.PlaySfx(PopupManager.Instance.myInfoAudio.sfx_Popup);
		// }
		if (toasts == null)
		{
			toasts = new List<ToastController>();
		}
		for (int i = 0; i < toasts.Count; i++)
		{
			toasts[i].SetSpeedUp();
		}
		toasts.Add(_tmpPopup);
		AddPopupActive(_tmpPopup);
	}
	public void RemovePopupActive(IPopupController _popup){
		if(popupsActive.Count == 0){
			return;
		}
		popupsActive.Remove(_popup);
		_popup.SelfDestruction();
		if (popupsActive.Count == 0) {
			popupCanvasGroup.alpha = 0f; 
			popupCanvasGroup.blocksRaycasts = false;
		}
	}
	public void UnActiveAllPopups(){
		if(popupsActive != null && popupsActive.Count > 0){
			for(int i = 0; i < popupsActive.Count; i++){
				popupsActive[i].SelfDestruction();
				popupsActive.RemoveAt(i);
				i--;
			}
			popupsActive.Clear ();
		}
		if (toasts != null && toasts.Count > 0)
		{
			toasts.Clear();
		}
		popupCanvasGroup.alpha = 0f;
		popupCanvasGroup.blocksRaycasts = false;
	}
	public void DestroyAll(){
		UnActiveAllPopups();
		int _poolChildCount = pool.childCount;
		for (var i = _poolChildCount - 1; i >= 0; i--){
			Destroy(pool.GetChild(i).gameObject);
		}
	}
    public void SetCanvasPopupAgain(){ 
		if (myCanvas.worldCamera == null){
			myCanvas.worldCamera = Camera.main;
		}
		myCanvas.sortingLayerName = "Canvas";
		popupCanvasGroup.alpha = 1f;
		popupCanvasGroup.blocksRaycasts = true;
	}
	public PopupFinishGameController CreatePopupFinishGame(int _levelNumber,System.Action _restart) {
		SetCanvasPopupAgain();
		PopupFinishGameController _tmpPopup = LeanPool.Spawn(popupFinishGamePrefab, Vector3.zero, Quaternion.identity, popupContainer, pool);
		_tmpPopup.Init(_levelNumber, () => {
			_restart();
			RemovePopupActive(_tmpPopup);
		});
        MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_Popup);
        AddPopupActive(_tmpPopup);
		return _tmpPopup;
	}
	public PopupKnifeShopController CreatePopupShop(int _itemCount,System.Action _onclose)
	{
		SetCanvasPopupAgain();
		PopupKnifeShopController _tmpPopup = LeanPool.Spawn(popupKnifeShopPrefab, Vector3.zero, Quaternion.identity, popupContainer, pool);
		_tmpPopup.Init(_itemCount,()=> {
			_onclose();
			RemovePopupActive(_tmpPopup);
		});
        MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_Popup);
        AddPopupActive(_tmpPopup);
		return _tmpPopup;
	}
	public PopupSettingController CreatePopupSetting()
	{
		SetCanvasPopupAgain();
		PopupSettingController _tmpPopup = LeanPool.Spawn(popupSettingPrefab, Vector3.zero, Quaternion.identity, popupContainer, pool);
		_tmpPopup.Init(() => {
			RemovePopupActive(_tmpPopup);
		});
		MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_Popup);
		AddPopupActive(_tmpPopup);

		return _tmpPopup;
	}
	#region For Test
	[Button] void TestDestroyAll(){
		UnActiveAllPopups();
		int _poolChildCount = pool.childCount;
		for (var i = _poolChildCount - 1; i >= 0; i--){
			Destroy(pool.GetChild(i).gameObject);
		}
	}
    #endregion
}