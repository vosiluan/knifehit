﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.SceneManagement;

public class CoreGameManager : MonoBehaviour {
    public static CoreGameManager instance
    {
        get
        {
            return ins;
        }
    }
    private static CoreGameManager ins;

    public class CallbackManager {
        public System.Action onDestroyAllSingletonCreated;
        public System.Action onLoadWhenGetFirstDataSucessful;

        public System.Action onUpdateOwnGold;
    }
    public CallbackManager callbackManager;
    public TypeOS currentOS;
    public enum TypeOS { 
        Android,
        Ios,
        WebGL,
        Win
    }
    [Header("Game Info")]
    public SingletonPrefabInfo singletonPrefabInfo;
    public List<Sprite> listSpriteKnife; 
    List<IEnumerator> listProcessAPI;
    IEnumerator actionJoinGameWithFriend;
    bool isPaused = false;
    void Awake(){
        if (ins != null && ins != this)
        {
            Destroy(this.gameObject);
            return;
        }
        ins = this;
        DontDestroyOnLoad(this.gameObject);
        Init();
    }
    void Init() {
        #if UNITY_ANDROID
            currentOS = TypeOS.Android;
        #elif UNITY_IOS
            currentOS = TypeOS.Ios;
        #elif UNITY_WEBGL
            currentOS = TypeOS.WebGL;
        #else
            currentOS = TypeOS.Win;
        #endif

        #if TEST
        Debug.Log("<color=green>Current OS ==> </color>" + currentOS.ToString());
        #endif
        
        callbackManager = new CallbackManager();
        DataManager.Init();
        LeanTween.init(9999);
        Application.targetFrameRate = 60;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        if(currentOS == TypeOS.Android || currentOS == TypeOS.Win){
            onKeyBackClicked = new List<System.Action>();
            StartCoroutine(DoActionCheckPressBackKey());
        }
    }
    void Start() {
        listProcessAPI = new List<IEnumerator>();
        StartCoroutine(DoActionRunProcessAPI());
    }
    void OnApplicationQuit()
    {
        DataManager.SaveData();
    }
    void OnApplicationFocus(bool _hasFocus)
    {
        isPaused = !_hasFocus;
        if (isPaused)
        {
            #if !UNITY_EDITOR
            DataManager.SaveData();
            #endif
        }
        // Debug.Log(">>> OnApplicationFocus: " + isPaused);
    }
    void OnApplicationPause(bool _pauseStatus)
    {
        isPaused = _pauseStatus;
        // Debug.Log(">>> OnApplicationPause: " + isPaused);
    }

    #region Process API Dont Destroy OnLoad
    IEnumerator DoActionRunProcessAPI(){
		while(true){
			if(listProcessAPI.Count == 0){
				yield return new WaitUntil(()=> listProcessAPI.Count > 0);
			}
			yield return StartCoroutine(listProcessAPI[0]);
            listProcessAPI.RemoveAt(0);
		}
	}
    #endregion
    #region Utilities
    public void DoVibrate()
    {
        if (!SystemInfo.supportsVibration)
        {
            return;
        }
        if (DataManager.instance.vibrationStatus != 1)
        {
            return;
        }
#if UNITY_ANDROID || UNITY_IOS
		Handheld.Vibrate();
#endif
    }
    #endregion

    #if UNITY_WEBGL && !UNITY_EDITOR
    public void WebGLResizeAgain(int _w, int _h){
        Screen.SetResolution(_w, _h, Screen.fullScreen);
    }
    #endif

    #region Press back key
    public List<System.Action> onKeyBackClicked;
    bool canPressButtonEscape = true;
    System.DateTime nextTimeToPressBackKey;
    IEnumerator DoActionCheckPressBackKey()
    {
        nextTimeToPressBackKey = System.DateTime.UtcNow;
        while (true)
        {
            yield return null;
            if (canPressButtonEscape)
            {
                if (System.DateTime.UtcNow >= nextTimeToPressBackKey)
                {
                    if (Input.GetKeyUp(KeyCode.Escape))
                    {
                        if (onKeyBackClicked != null && onKeyBackClicked.Count > 0){

                            // if(LoadingCanvasController.IsExist() && LoadingCanvasController.instance.currentState == LoadingCanvasController.State.Show){
                            //     continue; 
                            // }

                            canPressButtonEscape = false;
                            try{
                                onKeyBackClicked[onKeyBackClicked.Count - 1]();
                            }catch (System.Exception e) {
                                #if TEST
                                Debug.LogError(SceneManager.GetActiveScene().name + ": " + e.StackTrace);
                                #endif
                                RemoveCurrentCallbackPressBackKey();
                            }
                            yield return new WaitForSecondsRealtime(0.2f);
                            canPressButtonEscape = true;
                        }
                    }
                }
            }
        }
    }
    public void RegisterNewCallbackPressBackKey(System.Action _onKeyBackClicked, float _timeDelay = 1f){
        if(currentOS != TypeOS.Android && currentOS != TypeOS.Win){
            return;
        }
		nextTimeToPressBackKey = System.DateTime.UtcNow.AddMilliseconds((double)(_timeDelay*1000f));
		onKeyBackClicked.Add(_onKeyBackClicked);
    }
    public void RemoveCurrentCallbackPressBackKey(System.Action _onKeyBackClicked = null){
        if(currentOS != TypeOS.Android && currentOS != TypeOS.Win){
            return;
        }		
        if(onKeyBackClicked == null || onKeyBackClicked.Count <= 0){
			return;
		}
		if (_onKeyBackClicked != null) {
			onKeyBackClicked.Remove (_onKeyBackClicked);
		}
		else {
			onKeyBackClicked.RemoveAt(onKeyBackClicked.Count - 1);
		}
    }
    public void ClearAllCallbackPressBackKey()
    {
        if(currentOS != TypeOS.Android && currentOS != TypeOS.Win){
            return;
        }
		nextTimeToPressBackKey = System.DateTime.UtcNow;
		if(onKeyBackClicked != null){
			onKeyBackClicked.Clear();
		}
    }
    #endregion
    [Button] public void ClearAllData(){
        DataManager.ClearData();
        PlayerPrefs.DeleteAll();
    }
}
