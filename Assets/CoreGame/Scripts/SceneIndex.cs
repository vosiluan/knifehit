﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SceneIndex
{
    Lobby_Scene = 0,
    Ingame_Scene = 1,
    Unknown = 999
}
