﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;

public class LoadingCanvasController : MonoBehaviour {
	public static LoadingCanvasController instance{
		get{
			if(ins == null){
				ins = Instantiate(CoreGameManager.instance.singletonPrefabInfo.loadingCanvasPrefab);
				DontDestroyOnLoad (ins.gameObject);
				CoreGameManager.instance.callbackManager.onDestroyAllSingletonCreated += ins.SelfDestruction;
			}
			return ins;
		}
	}
	static LoadingCanvasController ins;
	public static bool IsExist(){
		if(ins != null){
			return true;
		}
		return false;
	}
	
	public Global_Enum.PanelState currentState{ get; set;}

	[Header("Setting")]
	[SerializeField] CanvasGroup myCanvasGroup;
	[SerializeField] Canvas myCanvas;
	[SerializeField] Text txtLoading;
	[SerializeField] ParticleSystem loadingParticle;
	IEnumerator actionWaitToHide;
	System.Action onFinishedTimeOut, onForcedHideNow;
	
	int idTweenCanvasGroup = -1;
	bool isLock;
	System.DateTime timeCanPressButtonHide;

	void Awake(){
		if (ins != null && ins != this) {
			Destroy(this.gameObject);
			return;
		}
		Hide (true);
	}

	[Button] void TestShow(){
		Show();
	}
	public void SetText(string _str){
		txtLoading.text = _str;
	}
	public void Show(float _timeOut = -1f, bool _isLock = false, System.Action _onFinishedTimeOut = null, System.Action _onForcedHideNow = null){
		if(currentState == Global_Enum.PanelState.Show){
			return;
		}
		if(idTweenCanvasGroup != -1 && LeanTween.descr(idTweenCanvasGroup) != null){
			LeanTween.cancel(idTweenCanvasGroup);
		}
		if(myCanvas.worldCamera == null){
            myCanvas.worldCamera = Camera.main;
        }

		currentState = Global_Enum.PanelState.Show;
		isLock = _isLock;
		if(string.IsNullOrEmpty(txtLoading.text)){
			txtLoading.text = "Đang tải";
		}
		LayoutRebuilder.ForceRebuildLayoutImmediate(txtLoading.rectTransform);

		onForcedHideNow = _onForcedHideNow;
		onFinishedTimeOut = _onFinishedTimeOut;
		myCanvasGroup.alpha = 0f;
		myCanvasGroup.blocksRaycasts = true;
		
		idTweenCanvasGroup = LeanTween.alphaCanvas(myCanvasGroup, 1f, 0.2f).setOnComplete(()=>{
			idTweenCanvasGroup = -1;
		}).setOnStart(()=>{
			loadingParticle.gameObject.SetActive (true);
		}).setIgnoreTimeScale(true).setDelay(0.5f).id;
		
		if(_timeOut > 0f){
			if(actionWaitToHide != null){
				StopCoroutine(actionWaitToHide);
				actionWaitToHide = null;
			}
			actionWaitToHide = DoActionWaitToHide(_timeOut);
			StartCoroutine(actionWaitToHide);
		}
		timeCanPressButtonHide = System.DateTime.UtcNow.AddSeconds(0.5f);
	}

	IEnumerator DoActionWaitToHide(float _timeOut){
		yield return Yielders.Get(_timeOut);
		if(onFinishedTimeOut != null){
			onFinishedTimeOut();
			onFinishedTimeOut = null;
		}
		Hide();
	}

	public void OnButtonHide(){
		if(timeCanPressButtonHide > System.DateTime.UtcNow){
			return;
		}
		if(isLock){
			return;
		}
		if(onForcedHideNow != null){
			onForcedHideNow();
			onForcedHideNow = null;
		}
		Hide();
	}

	[Button] void TestHide(){
		Hide();
	}
	public void Hide(bool _forcedHide = false){
		if(!_forcedHide){
			if(currentState == Global_Enum.PanelState.Hide){
				return;
			}
		}
		
		StopAllCoroutines();
		actionWaitToHide = null;
		if(idTweenCanvasGroup != -1 && LeanTween.descr(idTweenCanvasGroup) != null){
			LeanTween.cancel(idTweenCanvasGroup);
		}
		idTweenCanvasGroup = -1;

		myCanvasGroup.alpha = 0f;
		myCanvasGroup.blocksRaycasts = false;
		txtLoading.text = string.Empty;
		loadingParticle.gameObject.SetActive (false);
		currentState = Global_Enum.PanelState.Hide;

		onFinishedTimeOut = null;
		onForcedHideNow = null;

		isLock = false;
	}
	public void SelfDestruction(){
		if(ins != null){
			CoreGameManager.instance.callbackManager.onDestroyAllSingletonCreated -= ins.SelfDestruction;
			Destroy(instance.gameObject); 
			ins = null;
		}
	}
}