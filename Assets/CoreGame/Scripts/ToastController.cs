﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToastController : IPopupController
{

	public static Vector3 posDefault = new Vector3(0f, 1.5f, 0f);
	public static float maxSizeWDefault = 800f;
	public static Color colorTextDefault = Color.white;

	enum State
	{
		None, Begin, Idle, End
	}
	State currentState;

	[SerializeField] Text textInfo;
	[SerializeField] RectTransform myRectTransformMainContent;
	int idTweenCavasGroup, idTweenMainContent, idTweenScaleText;

	[Header("Setting")]
	[SerializeField] float timeDefaulShowBegin;
	[SerializeField] float timeDefaulShowIdle;
	[SerializeField] float timeDefaulShowEnd;

	bool speedUp;
	float timeShowBegin, timeShowIdle, timeShowEnd;


	public override void ResetData()
	{
		base.ResetData();

		currentState = State.None;

		if (idTweenCavasGroup != -1 && LeanTween.descr(idTweenCavasGroup) != null)
		{
			LeanTween.cancel(idTweenCavasGroup);
		}
		if (idTweenMainContent != -1 && LeanTween.descr(idTweenMainContent) != null)
		{
			LeanTween.cancel(idTweenMainContent);
		}
		if (idTweenScaleText != -1 && LeanTween.descr(idTweenScaleText) != null)
		{
			LeanTween.cancel(idTweenScaleText);
		}
		idTweenCavasGroup = idTweenMainContent = idTweenScaleText = -1;

		myCanvasGroup.alpha = 0f;

		speedUp = false;
	}

	public void Init(string _textInfo, Color _colorText, float _maxSizeW, System.Action _onClose = null)
	{
		textInfo.text = _textInfo;
		textInfo.color = _colorText;
		onClose = _onClose;

		Vector2 _sz = myRectTransformMainContent.sizeDelta;
		_sz.x = _maxSizeW;
		myRectTransformMainContent.sizeDelta = _sz;

		speedUp = false;
		timeShowBegin = timeDefaulShowBegin;
		timeShowIdle = timeDefaulShowIdle;
		timeShowEnd = timeDefaulShowEnd;

		myCanvasGroup.alpha = 0f;
		textInfo.transform.localScale = new Vector3(0.8f, 0.6f, 1f);
		myRectTransformMainContent.transform.localPosition = Vector3.zero;

		idTweenCavasGroup = idTweenMainContent = idTweenScaleText = -1;

		Show();
	}

	public override void Show(System.Action _onFinished = null)
	{
		SetBegin();
	}

	void SetBegin()
	{
		currentState = State.Begin;

		if (idTweenCavasGroup != -1 && LeanTween.descr(idTweenCavasGroup) != null)
		{
			LeanTween.cancel(idTweenCavasGroup);
		}
		idTweenCavasGroup = LeanTween.alphaCanvas(myCanvasGroup, 1f, timeShowBegin).setOnComplete(() =>
		{
			idTweenCavasGroup = -1;
		}).id;

		if (idTweenScaleText != -1 && LeanTween.descr(idTweenScaleText) != null)
		{
			LeanTween.cancel(idTweenScaleText);
		}
		idTweenScaleText = LeanTween.scale(textInfo.gameObject, Vector3.one, timeShowBegin).setOnComplete(() =>
		{
			idTweenScaleText = -1;
		}).id;

		if (idTweenMainContent != -1 && LeanTween.descr(idTweenMainContent) != null)
		{
			LeanTween.cancel(idTweenMainContent);
		}
		idTweenMainContent = LeanTween.moveLocalY(myRectTransformMainContent.gameObject, 5f, timeShowBegin).setOnComplete(() =>
		{
			idTweenMainContent = -1;
			SetIdle();
		}).id;
	}

	void SetIdle()
	{
		currentState = State.Idle;

		if (idTweenMainContent != -1 && LeanTween.descr(idTweenMainContent) != null)
		{
			LeanTween.cancel(idTweenMainContent);
		}
		idTweenMainContent = LeanTween.moveLocalY(myRectTransformMainContent.gameObject, 20f, timeShowIdle).setOnComplete(() =>
		{
			idTweenMainContent = -1;
			SetEnd();
		}).id;
	}

	void SetEnd()
	{
		currentState = State.End;

		if (idTweenCavasGroup != -1 && LeanTween.descr(idTweenCavasGroup) != null)
		{
			LeanTween.cancel(idTweenCavasGroup);
		}
		idTweenCavasGroup = LeanTween.alphaCanvas(myCanvasGroup, 0f, timeShowEnd).setOnComplete(() =>
		{
			idTweenCavasGroup = -1;
			Close();
		}).id;
	}

	public void SetSpeedUp()
	{
		if (speedUp)
		{
			return;
		}
		speedUp = true;
		timeShowBegin /= 2;
		timeShowIdle /= 2;
		timeShowEnd /= 2;

		if (currentState == State.Begin)
		{
			SetBegin();
		}
		else if (currentState == State.Idle)
		{
			SetIdle();
		}
		else if (currentState == State.End)
		{
			SetEnd();
		}
		else
		{
			Debug.LogError("BUG LOGIC!");
			SetEnd();
		}
	}

	public override void Close()
	{
		if (onClose != null)
		{
			onClose();
			onClose = null;
		}
		SelfDestruction();
	}
}