﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SingletonPrefabInfo", menuName="GameInfo/SingletonPrefabInfo")]
public class SingletonPrefabInfo : ScriptableObject {
    public PopupManager popupManagerPrefab;
    public LoadingCanvasController loadingCanvasPrefab;
    public SceneLoaderManager sceneLoaderManagerPrefab;
    public MyAudioManager myAudioManagerPrefab;

    [System.Serializable] 
    public class GlobalAudioInfo{
        public AudioClip Bgm;
        public AudioClip sfx_ClickButton;
        public AudioClip sfx_Popup;
        public AudioClip sfx_Swoosh;
        public AudioClip sfx__RockSmash;
        public AudioClip sfx__RockExplode;
        public AudioClip sfx__KnifeHit;
        public AudioClip sfx__Lose;
    }
    public GlobalAudioInfo globalAudioInfo;
}