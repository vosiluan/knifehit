﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupSettingController : IPopupController {
	[Header("Music")]
	[SerializeField] Toggle music_Toggle;

	[Header("Sfx")]
	[SerializeField] Toggle sound_Toggle;

	[Header("Vibration")]
	[SerializeField] Toggle vibration_Toggle;

	[Header("Grabphics")]
	[SerializeField] Slider grabphics_Slider;

	public override void ResetData(){
		base.ResetData();
    }
    public void Init(System.Action _onClose = null){
		/*Đọc data setting đã lưu lên và setting lại*/
		if (DataManager.instance.musicStatus == 1)
			music_Toggle.isOn = true;
		else
			music_Toggle.isOn = false;
		if (DataManager.instance.sfxStatus == 1)
			sound_Toggle.isOn = true;
		else
			sound_Toggle.isOn = false;
		//if (DataManager.instance.vibrationStatus == 1)
		//	vibration_Toggle.isOn = true;
		//else
		//	vibration_Toggle.isOn = false;
		onClose = _onClose;
		CoreGameManager.instance.RegisterNewCallbackPressBackKey(OnBtnCloseClicked);
    }
    #region On Button Clicked
    public void OnClickToggleMusic(bool _isOn){
		/*Tắt nhạc*/
		MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_ClickButton);
		music_Toggle.isOn = _isOn;
		if (music_Toggle.isOn)
		{
			DataManager.instance.musicStatus = 1;
			MyAudioManager.instance.ResumeMusic();
		}
		else
		{
			DataManager.instance.musicStatus = 0;
			MyAudioManager.instance.StopMusic();
		}
	}
	public void OnClickToggleSfx(bool _isOn){
		/*Tắt âm thanh*/
		MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_ClickButton);
		sound_Toggle.isOn = _isOn;
		if (sound_Toggle.isOn)
		{
			DataManager.instance.sfxStatus = 1;
			//MyAudioManager.instance.ResumeMusic();
		}
		else
		{
			DataManager.instance.sfxStatus = 0;
			//MyAudioManager.instance.StopMusic();
		}
	}
	public void OnClickToggleVibration(bool _isOn){
		/*Tắt rung*/
		MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_ClickButton);
		vibration_Toggle.isOn = _isOn;
		if (vibration_Toggle.isOn)
			DataManager.instance.vibrationStatus = 1;
		else
			DataManager.instance.vibrationStatus = 0;
	}
	public void OnBtnCloseClicked()
	{
		MyAudioManager.instance.PlaySfx(CoreGameManager.instance. singletonPrefabInfo.globalAudioInfo.sfx_ClickButton);
		Hide(() => {
			Close();
		});
	}
	public override void Close(){
		if (onClose != null){
			onClose();
			onClose = null;
		}
		SelfDestruction();
	}
	#endregion
}
