﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using NaughtyAttributes;

[RequireComponent(typeof(Rigidbody2D))]
public class Explodable : MonoBehaviour
{
    public string sortingLayerName = "Default";
    public int orderInLayer = 0;
    public List<GameObject> fragments = new List<GameObject>();
    public void explode()
    {
        foreach (GameObject frag in fragments)
        {
            frag.transform.parent = gameObject.transform.parent.transform;
            frag.SetActive(true);
            frag.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            frag.GetComponent<Rigidbody2D>().gravityScale = 1;
        }
        if (fragments.Count > 0)
        {
            gameObject.SetActive(false);
        }
    }
    void OnCollisionEnter2D(Collision2D _collision){
        if (_collision.collider.tag == "Knife"){
            fragments.Add(_collision.gameObject);
        }
    }
}
