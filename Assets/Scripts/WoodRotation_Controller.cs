﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodRotation_Controller : MonoBehaviour
{
    [SerializeField] CanvasGroup myCanvasGroup;
    [SerializeField] Transform mainContainer;
    public ShakeController shakeController;
    public int knifeCount;
    [System.Serializable]
    class RotationElement{
        public float Speed;
        public float Duration;
    }
    [SerializeField] RotationElement[] rotationPattern;
    public Explodable _explodable;
    public ExplosionForce ef;
    int idTweenCanvasGroup = -1;
    int idTweenMainContainer = -1;

    WheelJoint2D wheelJoint;
    JointMotor2D motor2D;
    IEnumerator actionWoodRotation;
    public void InitWood() {
        wheelJoint = GetComponent<WheelJoint2D>();
        motor2D = new JointMotor2D();
        StartRotationWood();
    }
    void StartRotationWood() {
        if (actionWoodRotation != null)
        {
            StopCoroutine(actionWoodRotation);
            actionWoodRotation = null;
        }
        actionWoodRotation = ActionRotationWood();
        StartCoroutine(actionWoodRotation);
    }
    IEnumerator ActionRotationWood()
    {
        int rotationIndex = 0;
        while (true)
        {
            yield return new WaitForFixedUpdate();
            motor2D.motorSpeed = rotationPattern[rotationIndex].Speed;

            motor2D.maxMotorTorque = 10000;
            wheelJoint.motor = motor2D;
            yield return new WaitForSecondsRealtime(rotationPattern[rotationIndex].Duration);
            /*Quay xong thì chuyển kiểu quay khác*/
            rotationIndex++;
            rotationIndex = rotationIndex < rotationPattern.Length ? rotationIndex : 0;
        }
    }
    public void Show(System.Action _onFinished = null)
    {
        if (idTweenCanvasGroup != -1 && LeanTween.descr(idTweenCanvasGroup) != null)
        {
            LeanTween.cancel(idTweenCanvasGroup);
        }
        if (idTweenMainContainer != -1 && LeanTween.descr(idTweenMainContainer) != null)
        {
            LeanTween.cancel(idTweenMainContainer);
        }

        transform.SetAsLastSibling();
        myCanvasGroup.alpha = 0f;
        myCanvasGroup.blocksRaycasts = true;

        idTweenCanvasGroup = LeanTween.alphaCanvas(myCanvasGroup, 1f, 0.2f).setEase(LeanTweenType.easeOutBack).setOnComplete(() =>
        {
            idTweenCanvasGroup = -1;
        }).setIgnoreTimeScale(true).id;
        mainContainer.localScale = Vector3.one * 0.6f;
        idTweenMainContainer = LeanTween.scale(mainContainer.gameObject, Vector3.one, 0.2f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            idTweenMainContainer = -1;
            if (_onFinished != null)
            {
                _onFinished();
            }
        }).setIgnoreTimeScale(true).id;
    }
    public void BrokenWood()
    {
        _explodable.explode();
        ef = GameObject.FindObjectOfType<ExplosionForce>();
        ef.doExplosion(_explodable.transform.position);
    }
}
