﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife_Controller : MonoBehaviour
{
    [SerializeField] Vector2 throwForce;
    bool isActive = true;
    [SerializeField] Rigidbody2D rb;
    [SerializeField] BoxCollider2D knifeCollider;
    [SerializeField] ParticleSystem woodHitEffect;
    [SerializeField] SpriteRenderer mySprite;
    public void Init(){
        mySprite.sprite = CoreGameManager.instance.listSpriteKnife[DataManager.instance.knifeID];
    }
    void OnCollisionEnter2D(Collision2D _collision){
        if (!isActive)
            return;
        isActive = false;
        if (_collision.collider.tag == "Wood") {
            InGame_Controller.instance.woodRotation_Controller.shakeController.SetUpShakeLocalPoint(0.1f, 0.02f);
            rb.velocity = new Vector2(0, 0);
            rb.bodyType = RigidbodyType2D.Kinematic;
            transform.SetParent(_collision.collider.transform);
            knifeCollider.offset = new Vector2(knifeCollider.offset.x, -0.75f);
            knifeCollider.size = new Vector2(knifeCollider.size.x, 1f);
            woodHitEffect.gameObject.SetActive(true);
            woodHitEffect.Play();
            InGame_Controller.instance.OnSuccessfulKnifeHit();
        }
        else if (_collision.collider.tag == "Knife")
        {
            MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx__KnifeHit);
            /*Kết thúc game*/
            rb.velocity = new Vector2(rb.velocity.x, -2);
            InGame_Controller.instance.SetFinishGame(false);
        }
    }
    public void ThrowKnife() {
        if (isActive){
            MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_Swoosh);
            MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx__RockSmash);
            rb.AddForce(throwForce, ForceMode2D.Impulse);
            rb.gravityScale = 1;
            InGame_Controller.instance.SetUnActiveIconKnife();
            InGame_Controller.instance.currentKnife = null;
        }
    }
}
