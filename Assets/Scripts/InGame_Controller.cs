﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InGame_Controller : MonoBehaviour {
    public static InGame_Controller instance { get; private set; }
    [SerializeField] int knifeCount;
    [SerializeField] Transform mainContainer;

    [Header("Knife Spawn")]
    [SerializeField] Vector2 knifeSpawnPosition;
    [SerializeField] Knife_Controller knifeObject;
    public Knife_Controller currentKnife;
    public WoodRotation_Controller woodRotation_Controller;
    [SerializeField] List<WoodRotation_Controller> listLevel;
    [Header("Icon Knife Display")]
    [SerializeField] Transform panelKnife;
    [SerializeField] Image iconKnife;
    [SerializeField] List<Image> listIconKnife;
    [SerializeField] Sprite iconKnifeActive;
    [SerializeField] Sprite iconKnifeUnActive;
    IEnumerator actionFinishGame;
    int knifeIconIndexToChange, levelNumber;
    void Awake() {
        instance = this;
    }
    void Start() {
        Init();
        woodRotation_Controller.InitWood();
    }
    void Init() {
        currentKnife = null;
        listIconKnife = new List<Image>();
        knifeIconIndexToChange = 0;
        levelNumber = 0;
        woodRotation_Controller = Instantiate(listLevel[levelNumber], transform);
        /*Tạo số lượng iconKnife và Knife đầu tiên*/
        knifeCount = woodRotation_Controller.knifeCount;
        InitListIconKnife(knifeCount);
        SpawnKnife();
    }
    public void OnSuccessfulKnifeHit()
    {
        if (knifeCount > 0)
            SpawnKnife();
        else
            SetFinishGame(true);
    }
    void SpawnKnife()
    {
        knifeCount--;
        Knife_Controller _knife = Instantiate(knifeObject, knifeSpawnPosition, Quaternion.identity);
        _knife.Init();
        currentKnife = _knife;
    }
    public void SetFinishGame(bool _iswin)
    {
        if (actionFinishGame != null) {
            StopCoroutine(actionFinishGame);
            actionFinishGame = null;
        }
        actionFinishGame = HandleFinishGame(_iswin);
        StartCoroutine(actionFinishGame);
    }
    IEnumerator HandleFinishGame(bool _iswin)
    {
        if (_iswin){
            MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx__RockExplode);
            woodRotation_Controller.BrokenWood();
            yield return new WaitForSecondsRealtime(0.5f);
            StartNextLevel();
        }
        else {
            yield return new WaitForSecondsRealtime(0.5f);
            MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx__Lose);
            mainContainer.gameObject.SetActive(false);
            woodRotation_Controller.gameObject.SetActive(false);
            PopupFinishGameController _popupFinish = PopupManager.Instance.CreatePopupFinishGame(levelNumber+1,() => {
                RestartGame();
            });
        }
    }
    public void RestartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }
    public void StartNextLevel() {
        levelNumber++;
        if (levelNumber < listLevel.Count) {
            knifeIconIndexToChange = 0;
            ClearSceneForNextLevel();
            InitListIconKnife(knifeCount);
            SpawnKnife();
            woodRotation_Controller.InitWood();
        }
    }
    void ClearSceneForNextLevel() {
        Destroy(woodRotation_Controller.gameObject);
        woodRotation_Controller = Instantiate(listLevel[levelNumber],transform);
        woodRotation_Controller.Show();
        for (int i = 0;  i< listIconKnife.Count; i++) {
            Destroy(listIconKnife[i].gameObject);
        }
        listIconKnife.Clear();
        knifeCount = listLevel[levelNumber].knifeCount;
    }
    public void OnclickThrowKnife() {
        if (currentKnife != null) {
            currentKnife.ThrowKnife();
        }
    }
    public void InitListIconKnife(int _count)
    {
        for (int i = 0; i < _count; i++) {
            /*Mặc định khi tạo ra đã ở trạng thái Active*/
            Image _iconKnife = Instantiate(iconKnife, panelKnife);
            listIconKnife.Add(_iconKnife);
        }
    }
    public void SetUnActiveIconKnife(){
        if (knifeIconIndexToChange >= listIconKnife.Count)
            return;
        listIconKnife[knifeIconIndexToChange].sprite = iconKnifeUnActive;
        knifeIconIndexToChange++;
    }
}
