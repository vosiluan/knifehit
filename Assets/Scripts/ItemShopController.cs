﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemShopController : MonoBehaviour
{
    [SerializeField] Image myImg;
    [SerializeField] Transform shadow;
    public int itemID;
    System.Action actionItemClick;
    public void Init(int _itemID,Sprite _sprite,System.Action _actionItemClick) {
        myImg.sprite = _sprite;
        itemID = _itemID;
        actionItemClick = _actionItemClick;
    }
    public void OnBtnItemClick() {
        actionItemClick();
        DataManager.instance.knifeID = itemID;
    }
    public void SetActive() {
        shadow.gameObject.SetActive(false);
    }
    public void SetUnActive()
    {
        shadow.gameObject.SetActive(true);
    }
}
