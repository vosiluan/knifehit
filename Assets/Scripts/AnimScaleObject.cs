﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimScaleObject : MonoBehaviour {
    [SerializeField] float scaleFrom;
    [SerializeField] float scaleTo;
    [SerializeField] float timeWait;
    void Start(){
        AutoScale();
    }
    void AutoScale() {
        LeanTween.scale(gameObject,Vector3.one* scaleTo, 0.5f).setLoopPingPong(0);
        LeanTween.scale(gameObject,Vector3.one* scaleFrom, 0.5f).setLoopPingPong(1);
    }
}
