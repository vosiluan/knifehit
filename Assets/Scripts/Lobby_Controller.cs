﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lobby_Controller : MonoBehaviour
{
    [SerializeField] Image imgMyKnife;
    [SerializeField] Transform mainContainer;
    //public PopupSettingController panelSetting { get; set; }
    public PopupSettingController panelSetting
    {
        get
        {
            _popupSetting = PopupManager.Instance.CreatePopupSetting();
            //if (_popupSetting == null)
            //{
                
            //}
            return _popupSetting;
        }
    }
    PopupSettingController _popupSetting;
    void Start() {
        MyAudioManager.instance.PlayMusic(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.Bgm);
        imgMyKnife.sprite = CoreGameManager.instance.listSpriteKnife[DataManager.instance.knifeID];
    }
    public void OnBtnClickPlay() {
        MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_ClickButton);
        SceneManager.LoadScene((int)SceneIndex.Ingame_Scene,LoadSceneMode.Single);
    }
    public void OnBtnClickSetting()
    {
        panelSetting.Show();
    }
    public void OnBtnOpenKnifeSkin()
    {
        mainContainer.gameObject.SetActive(false);
        MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_ClickButton);
        PopupKnifeShopController _popupShop = PopupManager.Instance.CreatePopupShop(CoreGameManager.instance.listSpriteKnife.Count, () => {
            mainContainer.gameObject.SetActive(true);
            imgMyKnife.sprite = CoreGameManager.instance.listSpriteKnife[DataManager.instance.knifeID];
        });
    }
    public void OnBtnClickComingSoon()
    {
        MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_ClickButton);
        PopupManager.Instance.CreateToast("Comming soon");
    }   
}
