﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupKnifeShopController : IPopupController
{
	[SerializeField] List<ItemShopController> listItems;
	[SerializeField] ItemShopController itemPrefab;
	[SerializeField] Transform transformSpawnItem;
	[SerializeField] Image myKnifeImg;
	bool isInit = false;
	public override void ResetData()
	{
		base.ResetData();
	}
	public void Init(int _itemCount,System.Action _onClose)
	{
		if (!isInit) {
			isInit = true;
			listItems = new List<ItemShopController>();
			for (int i = 0; i < _itemCount; i++)
			{
				ItemShopController _item = Instantiate(itemPrefab, transformSpawnItem);
				_item.Init(i, CoreGameManager.instance.listSpriteKnife[i], () => {
					SetUnActiveAllItem();
					_item.SetActive();
					myKnifeImg.sprite = CoreGameManager.instance.listSpriteKnife[_item.itemID];
				});
				/*Active skin đang select*/
				if (DataManager.instance.knifeID == i)
				{
					_item.SetActive();
					myKnifeImg.sprite = CoreGameManager.instance.listSpriteKnife[_item.itemID];
				}
				else
					_item.SetUnActive();
				listItems.Add(_item);
			}
		}
		onClose = _onClose;
		Show();
	}
	void SetUnActiveAllItem() {
		for (int i = 0; i < listItems.Count; i ++) {
			listItems[i].SetUnActive();
		}
	}
	public void OnButtonCloseClicked()
	{
		MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_ClickButton);
		Hide(() => {
			onClose();
			Close();
		});
	}
}
