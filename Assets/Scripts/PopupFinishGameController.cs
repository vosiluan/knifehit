﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PopupFinishGameController : IPopupController
{
    [SerializeField] Text txtLevel;
    System.Action onRestart;
	public override void ResetData()
	{
		base.ResetData();
	}

	public void Init(int _levelNumber,System.Action _restart)
	{
		txtLevel.text = "LEVEL " + _levelNumber.ToString();
		onRestart = _restart;
		Show();
	}
	public void OnButtonRestartClicked()
	{
        MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_ClickButton);
        Hide(() => {
			onRestart();
			Close();
		});
	}
	public void OnButtonBackHomeClicked() {
		Hide(() => {
			MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_ClickButton);
			SceneManager.LoadScene((int)SceneIndex.Lobby_Scene, LoadSceneMode.Single);
			Close();
		});
	}
	public void OnButtonCommingSoonClicked(){
		MyAudioManager.instance.PlaySfx(CoreGameManager.instance.singletonPrefabInfo.globalAudioInfo.sfx_ClickButton);
		PopupManager.Instance.CreateToast("Comming soon");
	}
}
